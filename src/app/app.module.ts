import { Message } from 'primeng/components/common/api';
import { AlunoService } from './aluno/aluno.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

import { AppComponent } from './app.component';
import { CadastroAlunoComponent } from './aluno/cadastro-aluno/cadastro-aluno.component';
import { HttpModule } from '@angular/http';
import { MessageService } from 'primeng/components/common/messageservice';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PesquisaAlunoComponent } from './aluno/pesquisa-aluno/pesquisa-aluno.component';

@NgModule({
    declarations: [
        AppComponent,
        CadastroAlunoComponent,
        PesquisaAlunoComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        TableModule,
        HttpClientModule,
        InputTextModule,
        DialogModule,
        ButtonModule,
        HttpModule,
        MessageModule,
        MessagesModule
    ],
    providers: [
        AlunoService,
        MessageService],
    bootstrap: [AppComponent]
})
export class AppModule { }
