import { Aluno } from './aluno';
import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  eventEmitter = new EventEmitter<any>();

  constructor(private http: Http) { }

  listar(): Observable<any>{
   return this.http.get('http://localhost:8080/alunos')
    .pipe(map(response => response.json()));
  }

  salvar(aluno: Aluno): Observable<any>{
    return this.http.post('http://localhost:8080/alunos', aluno)
      .pipe(
        map(
          res => {
            console.log(res.json());
            this.eventEmitter.emit(this.listar());
            return res.json();

          }
    ));
  }





}
