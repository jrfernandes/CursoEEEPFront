import { AlunoService } from './../aluno.service';
import { Http } from '@angular/http';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Aluno } from '../aluno';

@Component({
  selector: 'app-pesquisa-aluno',
  templateUrl: './pesquisa-aluno.component.html',
  styleUrls: ['./pesquisa-aluno.component.css']
})
export class PesquisaAlunoComponent implements OnInit {

  alunos: Aluno[] = [];

  constructor(private alunoService: AlunoService) { }

  ngOnInit() {
    this.alunoService.eventEmitter.subscribe(
      res => {
        this.listar();
      }
    );
    this.listar();
     
  }

  listar() {
    this.alunoService.listar().subscribe(
      res => {
        this.alunos = res;
      },
      err => {
        console.log("erro "+err);
      }
    );
  }

}
