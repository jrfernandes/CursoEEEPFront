import { PesquisaAlunoComponent } from './../pesquisa-aluno/pesquisa-aluno.component';
import { AlunoService } from './../aluno.service';
import { Component, OnInit } from '@angular/core';
import { Aluno } from '../aluno';
import { Message } from 'primeng/components/common/api';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-cadastro-aluno',
  templateUrl: './cadastro-aluno.component.html',
  styleUrls: ['./cadastro-aluno.component.css']
})
export class CadastroAlunoComponent implements OnInit {

  aluno: Aluno;
  msg: Message[] = [];

  constructor(private alunoService: AlunoService) { 
    this.aluno = new Aluno();
  }

  ngOnInit() {
    
  }

  inserir(form: FormControl){
    this.msg = []
    this.alunoService.salvar(this.aluno).subscribe(
      aluno => {
        this.msg.push({ severity: 'success', summary: 'Sucesso', detail: 'Aluno cadastrada com sucesso' });
        this.alunoService.listar();
        
        // this.listar();
      },
      erro => this.msg.push(
        { severity: 'error', summary: 'Erro', detail: 'Aluno com cpf já cadastrado' }
      )
    );
    // this.aluno = new Aluno();
    form.reset()
  }

  // listar(){
  //   this.alunoService.listar()
  //     .then(
  //       alunos => {
  //         this.alunos = alunos;
  //       }
  //     )
  // }
}
